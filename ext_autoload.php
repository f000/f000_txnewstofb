<?php

// Register necessary classes with autoloader
return array(
	'tx_f000txnewstofb_publish' => t3lib_extMgm::extPath('f000_txnewstofb', 'class.tx_f000txnewstofb_publish.php'),
	'tx_f000txnewstofb_additionalfields' => t3lib_extMgm::extPath('f000_txnewstofb', 'class.tx_f000txnewstofb_additionalfields.php'),
);
?>
